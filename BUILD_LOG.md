## Running the application

### Run on server

Open a browser and enter ```45.64.126.93:8000/api/students```<br>

### Run on local

0. If you have yet to install ```virtualenv```, execute ```python -m pip install vitualenv```<br>
1. Run ```git clone https://gitlab.com/fillantrophy02/edunet-api.git```<br>
2. Set up a ```virtualenv``` environment by navigating to the ```EduNet API``` folder then running ```python -m venv env```<br>
3. Activate the virtual environment by running ```.\env\Scripts\activate```<br>
4. To install the necessary packages, run ```.\env\Scripts\pip install -r requirements.txt```<br>
5. Navigate to ```EduNet API/LMS_sub``` then run ```.\env\Scripts\python manage.py runserver```. Open a browser and enter ```128.0.0.1/api/students``` to check if the application is working.<br>


## Internal setup

### Create a local project

0. Follow steps 0, 2 and 3 of the 'Run on local' section directly above to set up a virtual environment<br>
1. Follow the [Djongo API guide](https://www.digitalocean.com/community/tutorials/how-to-build-a-modern-web-application-to-manage-customer-information-with-django-and-react-on-ubuntu-18-04) to build a project.<br>
2. Install the other necessary packages<br>
3. If using mongo atlas, run ```.\env\Scripts\pip install djongo``` and ```.\env\Scripts\pip install pymongo```. See [Atlas integration with Django](https://medium.com/@hrithviksood1/integrating-mongodb-atlas-with-django-using-djongo-962dfd1513eb) for a full guide.<br>
4. Run ```.\env\Scripts\pip freeze > requirements.txt``` to create list of dependencies to be installed.<br>
5. Run ```.\env\Scripts\python manage.py runserver```. Open a browser and enter ```128.0.0.1/api/students``` to check if the application is working.<br>

### Deploy on company's server (for Vy's laptop only)

0. Run ```ssh ubuntu@45.64.126.93```<br>
1. Run ```git pull https://gitlab.com/fillantrophy02/edunet-api.git``` to pull the git repo<br>
2. Run ```cd api```<br>
3. Run ```pip3 install -r requirements.txt``` to install all required packages<br>
4. Current pymongo settings are set to local mongodb client. To change it to mongo atlas, type ```cd /home/ubuntu/.local/lib/python3.6/site-packages/pymongo``` then ```nano mongo_client.py```. Change the ```HOST``` variable to ```mongodb+srv://haden:iXVXtgPpCDZi5XYZ@cluster0.2cxow.mongodb.net/edunet?retryWrites=true&w=majority```.<br>
5. Navigate to the current django project (```cd api/LMS_sub```) and type ```python3 manage.py runserver 0.0.0.0:8000```<br>
6. Open a browser and enter ```45.64.126.93:8000/api/students``` to check if server has been successfully deployed<br>

### Update code (for Vy's laptop only)

0. Run ```ssh ubuntu@45.64.126.93```<br>
1. Run ```git pull https://gitlab.com/fillantrophy02/edunet-api.git```<br>
2. Run ```screen -ls```, ```screen -r 28588``` then ```Ctrl+C``` to exit from screen<br>
3. Type ```screen``` then ```python3 manage.py runserver 0.0.0.0:8000``` to upload to screen<br>
2. Open a browser and enter ```45.64.126.93:8000/api/students``` to check if server is successfully updated<br>s