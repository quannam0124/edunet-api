from rest_framework import serializers
from .models import StudentRecCourse

class StudentRecCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRecCourse
        fields = ('rec_courses',)

class StudentRecCourseDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRecCourse
        fields = '__all__'