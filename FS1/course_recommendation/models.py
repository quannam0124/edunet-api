from djongo import models

# Create your models here.
class StudentRecCourse(models.Model):
    student_id = models.GenericObjectIdField()
    weak_KPs = models.JSONField()
    schedule = models.JSONField(default=[])
    rec_courses = models.JSONField()
    
        