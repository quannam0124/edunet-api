from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import StudentRecCourse
from .serializers import *
from .staticinfo import serverlink
from django.urls import reverse

import re
import requests
import json
from .backend.course_recommendation import CourseRecommendation
from bson.objectid import ObjectId

@api_view(['GET'])
def rec_courses(request, student_id, weak_KPs, schedules):
    """
    Get recommended courses for a student 
    Receive student_id (string) and weak_KPs (string)
    """
    # get all course list from LMS
    courses_data = requests.get(serverlink+'api/courses/').json()
    weak_KPs_list = weak_KPs.split(",")
    schedules_list = schedules.split(",")

    if request.method == 'GET':
        # pass course list to get_course
        obj = CourseRecommendation(weak_KPs=weak_KPs_list, schedules=schedules_list, courses_data=courses_data)
        rec_courses = obj.get_recommended_courses()
        
        # save rec course to database 
        courses = StudentRecCourse(student_id=ObjectId(student_id), 
                                    weak_KPs=weak_KPs_list, 
                                    schedule=schedules_list,
                                    rec_courses=rec_courses)
        courses.save()

        # return the rec courses 
        response_data = StudentRecCourseSerializer(courses)
        return Response(response_data.data)

# function to get the rec courses of one student 
# def get_courses(all_courses, student_id, tree_id):
#     courses = [17, 13, 21]
#     # replace the actual function here 
#     return courses