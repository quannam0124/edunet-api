# Import pandas, numpy & cosine_similarity
import pandas as pd
import numpy as np
import json, os, re
from sklearn.metrics.pairwise import cosine_similarity
import pymongo

class CourseRecommendation:
    def __init__(self, courses_data):
        self.list_of_KPs = self.load_KPs() # Python list, e.g. [1.2.9.1.1, etc]
        self.courses_data = courses_data
        self.list_of_courses = self.load_courses() # Pandas dataframe, e.g. {{'id': 1, 'corr_KPs': ['1.2.1.1.1']}, {'id': 3, 'corr_KPs': ['1.9.2.2.2']}}

    def load_KPs(self):
        list_of_KPs = []
        r_exp = '^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]$'
        query = {'tree_id': {'$regex': r_exp}}
        details = {'tree_id': 1}

        creds = 'mongodb+srv://haden:iXVXtgPpCDZi5XYZ@cluster0.2cxow.mongodb.net/edunet?retryWrites=true&w=majority'
        client = pymongo.MongoClient(creds, 27017)
        db = client.get_database('edunet')
        coll = db.get_collection('students_kp')
        result = coll.find(query, details)
        for doc in result:
            list_of_KPs.append(doc.get('tree_id'))

        return list_of_KPs

    def load_courses(self):
        # Load courses from api here
        # data is now a python object
        # This is just a replacement
        # here = os.path.dirname(os.path.abspath(__file__))
        # with open(os.path.join(here, 'untitled.json')) as f:
        #     data = json.load(f) # data is a python object
        # data = json.load(courses_data)
        data = self.courses_data 
        
        # Real code starts here
       
        course_list = []
        for course in data:
            course_KPs = [x['tree_id'] for x in course['corr_KPs']]
            course_list.append([course['_id'], course_KPs])
        KP_courses = pd.DataFrame(columns=['course_id', 'KP_list'], data=course_list)

        return KP_courses

    def retrieve_student_vector(self, weak_KPs):
        """Converting a specific student's Weak KPS into matrix form
        
        Parameters
        ----------
        weak_KPs: list of strings
            A list of Weak Knowledge Points for a particular Student
            (e.g. ['1.1.1.1', '1.1.1.2'])

        Returns
        -------
        list of strings
            (need description of the returns)
        """
        list_of_student_WPs = []
        for a_knowledge_point in self.list_of_KPs:
            if a_knowledge_point in weak_KPs:
                list_of_student_WPs.append(1)
            else:
                list_of_student_WPs.append(0)

        return list_of_student_WPs

    def get_rec_courses(self, weak_KPs):
        """Getting recommended courses for the Students using Weak KPs
        
        Parameters
        ----------
        weak_KPs: list of strings
            A list of Weak Knowledge Points for a particular Student
            (e.g. ['1.1.1.1', '1.1.1.2'])

        Returns
        -------
        list of strings
            (need description of the returns)
        """            
        #Load Students' Database
        student_vector = self.retrieve_student_vector(weak_KPs)
        student_df = pd.DataFrame(columns=self.list_of_KPs, data=[student_vector], index=['dummy_id'])

        KP_courses = self.list_of_courses
        course_df = pd.DataFrame(columns=self.list_of_KPs, index = KP_courses.course_id)

        def convert_course_KPs_into_binary_matrix():
            """Converting KP_courses into a binary matrix
            
            Parameters
            ----------
            N.A

            Returns
            -------
            pandas DataFrame
                a binary matrix
                    1st column = course ID 
                    subsequent columns: {heading: every KP in database (e.g. 1.1.1.1), data: 1 or 0}
            """

            for i in range(len(course_df)):
                approved = []
                m = KP_courses['KP_list'][i]
                n = KP_courses['course_id'][i]

                for b in self.list_of_KPs:
                    if b in m:
                        approved.append(1)
                    else:
                        approved.append(0)
                course_df.loc[n] = approved

            return course_df

        convert_course_KPs_into_binary_matrix()
        
        def get_recommendations():

            # Adding a row of Student WPs into course_df for recommendation            
            # course_df.iloc[len(course_df)] = student_vector
            course_df.iloc[len(course_df)-1] = student_vector
            # course_df.append(student_df, ignore_index=True)
      
            # Using cosine similarity
            cosine_sim = cosine_similarity(course_df)
            
            # Deleting the row of Student WPs into course_df for recommendation            
            course_df.drop(course_df.tail(1).index, inplace = True)
            print(course_df)

            # Get the pairwise similarity scores of the last row i.e. Student WPs
            similarity_score = list(enumerate(cosine_sim[-1]))
            print(similarity_score)
            
            # Sort the courses based on the similarity scores
            similarity_score = sorted(similarity_score, key=lambda x: x[1], reverse=True)
            
            # Get the scores of the 10 most suitable courses
            similarity_score = similarity_score[1:11]
            
            # Get the indices of the 10 most recommended course 
            course_indices = [i[0] for i in similarity_score]
            print(course_indices)
            print(course_df.iloc[course_indices])
            
            # Return the top 10 most recommended courses
            return course_df.iloc[course_indices].index.tolist()

        return get_recommendations()
