# Import pandas, numpy & cosine_similarity
import numpy as np
import pandas as pd
import json, os, re
from sklearn.metrics.pairwise import cosine_similarity
import pymongo
# from utils import load_from_mongodb

class CourseRecommendation():

    def __init__(self, weak_KPs, 
                    schedules, 
                    courses_data,
                    number_of_top_recommended_courses = 10, 
                    sub_number_of_top_recommended_courses = 50, 
                    basic_level_of_KP_coefficient = 0.5):
        self.weak_KPs = weak_KPs
        self.schedules = schedules
        self.number_of_top_recommended_courses = number_of_top_recommended_courses
        self.sub_number_of_top_recommended_courses = sub_number_of_top_recommended_courses
        self.basic_level_of_KP_coefficient = basic_level_of_KP_coefficient

        self.courses_data = courses_data

    def load_KPs(self):
        """Load all available Knowledge Points from Database
        
        Parameters
        ----------
        N.A.

        Returns
        -------
        self.list_of_KPs: list of strings
            A list of all KPs available in the database
            (e.g. ['1.2.9.1.1', '1.9.1.2.3', etc.])
        """
        self.list_of_KPs = []
        r_exp = '^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]$'
        query = {'tree_id': {'$regex': r_exp}}
        details = {'tree_id': 1}

        creds = 'mongodb+srv://haden:iXVXtgPpCDZi5XYZ@cluster0.2cxow.mongodb.net/edunet?retryWrites=true&w=majority'
        client = pymongo.MongoClient(creds, 27017)
        db = client.get_database('edunet')
        coll = db.get_collection('students_kp')
        result = coll.find(query, details)
        for doc in result:
            self.list_of_KPs.append(doc.get('tree_id'))
    
    def load_schedules(self):
        """Load all available Schedules from Database
        
        Parameters
        ----------
        N.A.

        Returns
        -------
        self.list_of_schedules: list of strings
            A list of all Schedules available in the database
            (e.g. ['monday_evening', 'thursday_afternoon', etc.])
        """
        self.list_of_schedules = []

        query = {}
        details = {'dummyPeriod': 1}
        creds = 'mongodb+srv://haden:iXVXtgPpCDZi5XYZ@cluster0.2cxow.mongodb.net/edunet?retryWrites=true&w=majority'
        client = pymongo.MongoClient(creds, 27017)
        db = client.get_database('kayla')
        coll = db.get_collection('Schedule')
        result = coll.find(query, details)
        for doc in result:
            self.list_of_schedules = doc.get('dummyPeriod')

    def load_courses(self):
        """Load all available Courses from API
        
        Parameters
        ----------
        N.A.

        Returns
        -------
        self.df_of_courses: pandas dataframe
            A dataframe contains Courses information,
                each row corresponds to a Course,
                each column corresponds to a Course's Feature
            (e.g. {
              {'id': 1, 'corr_KPs': ['1.2.1.1.1', '1.3.1.1.1', etc.], 'schedule': ['monday_morning', 'wednesday_evening', etc.], 'price': 200000},
              {'id': 3, 'corr_KPs': ['1.9.2.2.2', '1.9.3.2.1', '1.9.3.2.2', etc.], 'schedule': ['tuesday_afternoon', etc.], 'price': 250000},
              etc.
            })
        """
        # query = {}
        # details = {"_id": 0}
        # result = load_from_mongodb('kayla', 'BananaDummyCourse', query, details)

        # course_list = []
        # for course in result:
        #     course_list.append([course.get('id'), course.get('corr_KPs'), course.get('schedule'), course.get('price')])

        course_list = []
        for course in self.courses_data:
            course_KPs = [x['tree_id'] for x in course['corr_KPs']]
            course_schedule = course['schedule'][1:-1].replace(' ', '').replace('\'','').replace('\"','').split(',')
            course_list.append([course['_id'], course_KPs, course_schedule, course['price']])

        self.df_of_courses = pd.DataFrame(columns=['course_id', 'KP_list', 'schedule', 'price'], data=course_list)
        # print(self.df_of_courses)

    def vectorize_course_KPs(self):
        """Converting Courses' KPs into a binary matrix
        
        Parameters
        ----------
        N.A

        Returns
        -------
        self.courses_KP_matrix: 2D numpy integer array
            A binary matrix contains Courses' KPs characteristic vectors,
                each row corresponds to a Course KP vector,
                each column corresponds to a KP in a list of KPs
            (e.g. [
              [0, 1, 1, 0, 0, etc.],
              [1, 0, 0, 0, 0, etc.],
              etc.
            ])
        """
        self.courses_KP_matrix = np.zeros((1, len(self.list_of_KPs)))

        for course in range(len(self.df_of_courses)):
            course_KP_list = self.df_of_courses['KP_list'][course]
            course_KP_vector = []

            for a_KP in self.list_of_KPs:
                if a_KP in course_KP_list:
                    course_KP_vector.append(1)
                else:
                    course_KP_vector.append(0)
            
            self.courses_KP_matrix = np.concatenate((self.courses_KP_matrix, [course_KP_vector]), axis=0)

        self.courses_KP_matrix = np.delete(self.courses_KP_matrix, 0, 0)

    def vectorize_course_schedules(self, sub_recommended_course_indices):
        """Converting Courses' Schedules into a binary matrix
        
        Parameters
        ----------
        sub_recommended_course_indices: list of integers
            A list of Sub-recommended Courses' Indices, obtained after matching Courses with the Student based on KPs
            (e.g. [5, 6, 43, 1, 8, etc.])

        Returns
        -------
        courses_schedule_matrix: 2D numpy integer array
            A binary matrix contains Courses' Schedules characteristic vectors,
                each row corresponds to a Course Schedule vector,
                each column corresponds to a period in the list of schedules
            (e.g. [
              [1, 0, 1, 0, 0, etc.],
              [0, 1, 0, 1, 1, etc.],
              etc.
            ])
        """
        courses_schedule_matrix = np.zeros((1, len(self.list_of_schedules)))

        for index in sub_recommended_course_indices:
            course_schedule_list = self.df_of_courses['schedule'][index]
            course_schedule_vector = []

            for a_period in self.list_of_schedules:
                if a_period in course_schedule_list:
                    course_schedule_vector.append(1)
                else:
                    course_schedule_vector.append(0)
            
            courses_schedule_matrix = np.concatenate((courses_schedule_matrix, [course_schedule_vector]), axis=0)

        courses_schedule_matrix = np.delete(courses_schedule_matrix, 0, 0)

        return courses_schedule_matrix

    def vectorize_student_weak_KPs_and_assessments(self):
        """Converting a specific Student's Weak KPs with Assessments into vector form
        
        Parameters
        ----------
        weak_KPs: dictionary
            A list of Weak Knowledge Points with Assessments for a particular Student
            (e.g. {
              "1.1.1.1": 0.345435,
              "1.1.1.2": 0.954633,
              etc.
            })

        Returns
        -------
        self.student_weak_KP_vector: list of integers
            A binary array contains Student's KP characteristic vector,
                each column corresponds to a KP in a list of KPs
            (e.g. [0, 1, 1, 0, 0, etc.])
        
        self.student_assessment_vector: list of floats
            An array contains Student's Assessment characteristic vector,
                each column corresponds to an Assessment to a corresponding KP in a list of KPs
            (e.g. [0.545765, 0.968758, 0., 0.132359, 0., etc.])
        """
        self.student_weak_KP_vector = []

        for a_KP in self.list_of_KPs:
            if a_KP in self.weak_KPs:
                self.student_weak_KP_vector.append(1)
            else:
                self.student_weak_KP_vector.append(0)
    
    '''
    def vectorize_student_weak_KPs_and_assessments(self):
        """Converting a specific Student's Weak KPs with Assessments into vector form
        
        Parameters
        ----------
        weak_KPs: dictionary
            A list of Weak Knowledge Points with Assessments for a particular Student
            (e.g. {
              "1.1.1.1": 0.345435,
              "1.1.1.2": 0.954633,
              etc.
            })

        Returns
        -------
        self.student_weak_KP_vector: list of integers
            A binary array contains Student's KP characteristic vector,
                each column corresponds to a KP in a list of KPs
            (e.g. [0, 1, 1, 0, 0, etc.])
        
        self.student_assessment_vector: list of floats
            An array contains Student's Assessment characteristic vector,
                each column corresponds to an Assessment to a corresponding KP in a list of KPs
            (e.g. [0.545765, 0.968758, 0., 0.132359, 0., etc.])
        """
        #self.weak_KPs = json.loads(self.weak_KPs)

        weak_KP_list = list(self.weak_KPs.keys())

        self.student_weak_KP_vector = []
        self.student_assessment_vector = []

        for a_KP in self.list_of_KPs:
            if a_KP in weak_KP_list:
                self.student_weak_KP_vector.append(1)

                index = weak_KP_list.index(a_KP)
                assessment_value = list(self.weak_KPs.values())[index]
                self.student_assessment_vector.append(assessment_value)
            else:
                self.student_weak_KP_vector.append(0)

                self.student_assessment_vector.append(0)
    '''

    def vectorize_student_schedules(self):
        """Converting a specific Student's Schedules into vector form
        
        Parameters
        ----------
        schedules: list of strings
            A list of periods the student is free to attend courses
            (e.g. ['monday morning', 'tuesday afternoon', 'wednesday evening', etc.])

        Returns
        -------
        self.student_schedule_vector: list of integers
            A binary array contains Student's Schedule characteristic vector,
                each column corresponds to a period in the list of schedules
            (e.g. [1, 0, 1, 1, 1, etc.])
        """
        self.student_schedule_vector = []

        for a_period in self.list_of_schedules:
            if a_period in self.schedules:
                self.student_schedule_vector.append(1)
            else:
                self.student_schedule_vector.append(0)
    
    def course_KP_vector_coefficient(self, course_KP_vector):
        """Generating coefficient for each Course KP Vector
        
        Parameters
        ----------
        course_KP_vector: list of integers
            A binary array contains Courses' Schedule characteristic vector,
                each column corresponds to a period in the list of schedules
            (e.g. [0, 1, 0, 1, 0, etc.])
        
        basic_level_of_KP_coefficient: float
            A parameter represented the basic level of a KP
            (e.g. 0.586783)

        Returns
        -------
        course_KP_vector_coefficient: float
            A characteristic coefficient for a Course's KP when interacting with Student's Assessment of that KP
            (e.g. 0.276589)
        """
        course_KP_vector_coefficient = 0

        for KP_element in range(len(self.list_of_KPs)):
            if course_KP_vector[KP_element] == 1:
                KP_weight = self.basic_level_of_KP_coefficient**KP_element
            else:
                KP_weight = 0
            course_KP_vector_coefficient += KP_weight/len(self.list_of_KPs)

        return course_KP_vector_coefficient

    '''
    def course_KP_vector_coefficient(self, course_KP_vector):
        """Generating coefficient for each Course KP Vector
        
        Parameters
        ----------
        course_KP_vector: list of integers
            A binary array contains Courses' Schedule characteristic vector,
                each column corresponds to a period in the list of schedules
            (e.g. [0, 1, 0, 1, 0, etc.])
        
        basic_level_of_KP_coefficient: float
            A parameter represented the basic level of a KP
            (e.g. 0.586783)

        Returns
        -------
        course_KP_vector_coefficient: float
            A characteristic coefficient for a Course's KP when interacting with Student's Assessment of that KP
            (e.g. 0.276589)
        """
        course_KP_vector_coefficient = 0

        for KP_element in range(len(self.list_of_KPs)):
            if course_KP_vector[KP_element] == 1:
                KP_weight = self.basic_level_of_KP_coefficient**KP_element * self.student_assessment_vector[KP_element]
            else:
                KP_weight = 0
            course_KP_vector_coefficient += KP_weight/len(self.list_of_KPs)

        return course_KP_vector_coefficient
    '''

    def get_recommended_courses(self):
        """Getting recommended Courses for the Student
        
        Parameters
        ----------
        weak_KPs: dictionary
            A list of Weak Knowledge Points with Assessments for a particular Student
            (e.g. {
              "1.1.1.1": 0.345435,
              "1.1.1.2": 0.954633,
              etc.
            })

        schedules: list of strings
            A list of periods the student is free to attend courses
            (e.g. ['monday morning', 'tuesday afternoon', 'wednesday evening', etc.])
        
        number_of_top_recommended_courses: integer
            Number of top Recommended Courses
            (e.g. 10)
        
        sub_number_of_top_recommended_courses: integer
            Number of top Recommended Courses obtained after matching Courses with the Student based on KP
            !Constrain: must bigger or equal to number_of_top_recommended_courses
            (e.g. 100)

        basic_level_of_KP_coefficient: float
            A parameter represented the basic level of a KP
            (e.g. 0.586783)

        Returns
        -------
        self.recommended_course_list: list of integers
            A list of indices of recommended courses
            (e.g. [55, 54, 87, 2, 9, etc.])
        """
        # Load Knowledge Points' Database
        self.load_KPs()

        # Load Schedules' Database
        self.load_schedules()

        # Load Courses' Database
        self.load_courses()

        # Vectorize Courses' Knowledge Points
        self.vectorize_course_KPs()
        
        # Vectorize Student's Weak Knowledge Points and Assessments
        self.vectorize_student_weak_KPs_and_assessments()
        
        # Vectorize Student's Schedules
        self.vectorize_student_schedules()

        # Make a list to store Recommended Courses
        self.recommended_course_list = []

        # Make a copy of the Courses' Knowledge Point Binary Matrix
        courses_KP_vector_available = self.courses_KP_matrix.copy()
        
        # All the Weak Knowledge Points have not been fully covered
        status = True

        #print(self.student_weak_KP_vector)
        #print(self.student_assessment_vector)

        while status:
            """The first filter layer:
                Get Sub-recommended Courses for the Student using Knowledge Points
            """
            # Make a list to store cosine similarity's sub-scores
            sub_score_list = []

            # Make a list to store cosine similarity's scores
            score_list = []

            # Iterate through available courses and using cosine similarity based on Courses' KPs
            for course_index in range(len(courses_KP_vector_available)):

                # Calculate the cosine similarity's score between Student's Weak KP vector and Course's KP vector
                sub_score = cosine_similarity([self.student_weak_KP_vector], [courses_KP_vector_available[course_index]])[0][0]

                # Score after adding Course's Coefficient
                score = self.course_KP_vector_coefficient(courses_KP_vector_available[course_index]) * sub_score

                # Add the score into the designated list
                sub_score_list.append(score)

            # Find indices of an indicated number of Sub-recommended Courses with highest score
            sub_recommended_course_indices = np.flip(np.argsort(sub_score_list)[-self.sub_number_of_top_recommended_courses:])

            # Vectorize Sub-recommended Courses' Schedules
            courses_schedule_matrix = self.vectorize_course_schedules(sub_recommended_course_indices)
            """The second filter layer:
                Get Recommended Courses for the Student using Schedules
            """
            # Iterate through Sub-recommended Courses and using cosine similarity based on Courses' Schedules
            for index in range(len(courses_schedule_matrix)):
                
                # Calculate the cosine similarity's score between Student's Schedule vector and Course's Schedule vector
                final_score = cosine_similarity([self.student_schedule_vector], [courses_schedule_matrix[index]])[0][0]

                # Add the score into the designated list
                score_list.append(final_score)
            
            # Find indices of an indicated number of Sub-recommended Courses' indices with highest score
            recommended_course_schedule_matrix_indices = np.flip(np.argsort(score_list)[-self.number_of_top_recommended_courses:])

            recommended_course_ids = []

            for index in recommended_course_schedule_matrix_indices:
                recommended_course_index = sub_recommended_course_indices[index]
                recommended_course_id = self.df_of_courses['course_id'][recommended_course_index]
                recommended_course_ids.append(recommended_course_id)

            """# Make a list to store indices of an indicated number of Recommended Courses
            recommended_course_indices = []

            # Indices of Recommended Courses
            for index in recommended_course_schedule_matrix_indices:
                recommended_course_indices.append(sub_recommended_course_indices[index])

            # Print Recommended Courses Indices list
            print(recommended_course_indices)
            print('\n')

            # Prompt user to choose a course from the Recommended Courses Indices list
            recommended_course_index = int(input('Choose recommended course index: '))
            print('\n')

            # Modify Student's Weak Knowledge Point and Assessment vectors, and available Courses
            for KP_element in range(len(self.list_of_KPs)):

                # Remove the covered Student's Weak KPs and Assessments, and chosen Course
                if self.student_weak_KP_vector[KP_element] == 1 and courses_KP_vector_available[recommended_course_index][KP_element] == 1:
                    self.student_weak_KP_vector[KP_element] = 0
                    #self.student_assessment_vector[KP_element] = 0
                    courses_KP_vector_available[recommended_course_index][KP_element] = 0

            #print(self.student_weak_KP_vector)
            #print(self.student_assessment_vector)

            # Update the status
            if 1 not in self.student_weak_KP_vector:
                status = False
            
            # Add the chosen Course into the Recommended Course path
            self.recommended_course_list.append(recommended_course_index)"""

            self.recommended_course_list = recommended_course_ids
            status = False

        """# Print the Recommended Course path
        print(self.recommended_course_list)"""

        return self.recommended_course_list
