from django.apps import AppConfig


class CourseRecommendationConfig(AppConfig):
    name = 'course_recommendation'
