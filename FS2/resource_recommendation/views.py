from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import StudentRecResource
from .serializers import *
from .staticinfo import serverlink
from django.urls import reverse

import re
import requests
import json
from .backend.resource_recommendation import ResourceRecommendation

@api_view(['GET'])
def rec_courses(request, student_id, weak_KPs):
    """
    Get recommended resources for a student 
    Receive student_id (string) and weak_KPs (string)
    """
    # get all resource list from LMS
    resource_data = requests.get(serverlink+'api/resource/').json()

    if request.method == 'GET':
        # pass course list to get_course
        tree_id = weak_KPs.split(",")

        obj = ResourceRecommendation(resource_data)
        rec_resources = obj.get_rec_resource(tree_id)
        
        # save rec course to database 
        resource = StudentRecResource(student_id=student_id, weak_KPs=tree_id, rec_resources=rec_resources)
        resourse.save()

        # return the rec courses 
        response_data = StudentRecResourceSerializer(resource)
        return Response(response_data.data)

