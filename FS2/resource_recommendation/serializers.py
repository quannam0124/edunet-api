from rest_framework import serializers
from .models import StudentRecResource

class StudentRecResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRecResource
        fields = ('rec_resource',)