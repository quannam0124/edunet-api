from djongo import models

# Create your models here.
class StudentRecResource(models.Model):
    student_id = models.IntegerField(default=0)
    weak_KPs = models.JSONField()
    rec_resources = models.JSONField()
        
class Resource(models.Model):
    
    class Category(models.IntegerChoices):
        VIDEO = 1
        BOOK = 2

    id = models.IntegerField(default=0, primary_key=True)
    corr_KP = models.JSONField()
    price = models.IntegerField()
    category = models.IntegerField(choices=Category.choices)
    name = models.CharField()
    link = models.URLField()
    rating = models.IntegerField(default=0)