from django.apps import AppConfig


class ResourceRecommendationConfig(AppConfig):
    name = 'resource_recommendation'
