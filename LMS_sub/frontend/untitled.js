import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class StudentsService{
    constructor(){}

    getStudents() {
        const url = `${API_URL}/api/students`;
        return axios.get(url).then(response => response.data);
    }
}

const studentService = new StudentsService();
console.log(studentService.getStudents());