"""EduNet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from students import views
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/students/$', views.students_list),
    url(r'^api/students/(?P<pk>[0-9a-zA-Z]+)$', views.students_detail),
    url(r'^api/KPs/$', views.KPs_list),
    url(r'^api/KPs/(?P<tree_id>[0-9\.]+)$', views.KPs_detail),
    url(r'^api/KPs/(?P<tree_id>[0-9\.]+)/subset/$', views.KPs_subset),
    url(r'^api/KPs-nested/$', views.KPs_nested),
    url(r'^api/courses/$', views.courses_list),
    url(r'^api/courses/(?P<pk>[0-9a-zA-Z]+)$', views.courses_detail),
]
