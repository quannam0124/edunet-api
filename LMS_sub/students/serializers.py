from rest_framework import serializers
from djongo import models
from .models import KP, KPSubject, KPGrade, KPArea1L, KPArea2L, KPArea3L
from .models import Course
from .models import Student
from .models import StudentKP
from .staticinfo import serverlink
from bson.objectid import ObjectId
import requests, json
import ast

class KPSerializer(serializers.ModelSerializer):
    class Meta:
        model = KP
        fields = ('tree_id', 'name')


class KPArea3LSerializer(serializers.ModelSerializer):
    class Meta:
        model = KPArea3L
        fields = ('area3L_id', 'name')

class KPArea2LSerializer(serializers.ModelSerializer):
    subset = KPArea3LSerializer(many=True, read_only=True)
    class Meta:
        model = KPArea2L
        fields = ('area2L_id', 'name', 'subset')

class KPArea1LSerializer(serializers.ModelSerializer):
    subset = KPArea2LSerializer(many=True, read_only=True)
    class Meta:
        model = KPArea1L
        fields = ('area1L_id', 'name', 'subset')

class KPGradeSerializer(serializers.ModelSerializer):
    subset = KPArea1LSerializer(many=True, read_only=True)
    class Meta:
        model = KPGrade
        fields = ('grade_id', 'name', 'subset')

class KPSubjectSerializer(serializers.ModelSerializer):
    subset = KPGradeSerializer(many=True, read_only=True)
    class Meta:
        model = KPSubject
        fields = ('subject_id', 'name', 'subset')


class CourseDisplaySerializer(serializers.ModelSerializer):
    corr_KPs = KPSerializer(read_only=True, many=True)

    class Meta:
        model = Course
        fields = ('_id', 'name', 'corr_KPs', 'price', 'duration', 'room', 'teacher_id', 'start_time', 'schedule')

class CourseCreateSerializer(serializers.ModelSerializer):
    corr_KPs = serializers.ListField(
        write_only=True,
        child=serializers.CharField(max_length=100)
    )

    class Meta:
        model = Course
        fields = ('_id', 'name', 'corr_KPs', 'price', 'duration', 'room', 'teacher_id', 'start_time', 'schedule')

    def create(self, validated_data):
        KPs_data = validated_data.pop('corr_KPs')

        teacher_id = validated_data.pop('teacher_id')
        teacher_id = ObjectId(teacher_id)

        _id = ObjectId()

        validated_data['_id'] = _id
        validated_data['teacher_id'] = teacher_id

        course = Course.objects.create(**validated_data)
        course._id = _id
        course.teacher_id = ObjectId(teacher_id)

        for tree_id in KPs_data:
            kp = KP.objects.get(tree_id=tree_id)
            course.corr_KPs.add(kp)
        course.save()

        return course

    def update(self, instance, validated_data):
        #fields = [f.name for f in Course._meta.get_fields()]
        if 'corr_KPs' in validated_data:
            KPs_data = validated_data['corr_KPs']
            for tree_id in KPs_data:
                kp = KP.objects.get(tree_id=tree_id)
                instance.corr_KPs.add(kp)
        
        if 'name' in validated_data:
            instance.name = validated_data.get('name', instance.name)
        if 'duration' in validated_data:
            instance.duration = validated_data.get('duration', instance.duration)
        if 'room' in validated_data:
            instance.room = validated_data.get('room', instance.room)
        if 'price' in validated_data:
            instance.price = validated_data.get('price', instance.price)
        if 'teacher_id' in validated_data:
            teacher_id = validated_data.get('teacher_id', instance.teacher_id)
            instance.teacher_id = ObjectId(teacher_id)
        if 'start_time' in validated_data:
            instance.start_time = validated_data.get('start_time', instance.start_time)
        if 'schedule' in validated_data:
            instance.schedule = validated_data.get('schedule', instance.start_time)

        instance.save()

        return instance

class CourseIdDisplaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('_id', )

class StudentKPSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentKP
        fields = ('tree_id', 'score')

# class KPForIntermediarySerializer(serializers.ModelSerializer):
#     studentkps = StudentKPSerializer(read_only=True, many=True)
#     class Meta:
#         model = KP
#         fields = ('tree_id', 'studentkps')

class StudentDisplaySerializer(serializers.ModelSerializer):
    kp = StudentKPSerializer(read_only=True, many=True)
    rec_courses = CourseIdDisplaySerializer(read_only=True, many=True)
    enrol_courses = CourseIdDisplaySerializer(read_only=True, many=True)

    class Meta:
        model = Student
        fields = ('_id', 'kp', 'rec_courses', 'enrol_courses')

class StudentCreateSerializer(serializers.ModelSerializer):
    kp = serializers.JSONField()
    enrol_courses = serializers.ListField(
        required=False,
        write_only=True,
        child=serializers.CharField(max_length=100)
    )

    class Meta:
        model = Student
        fields = ('_id', 'kp', 'enrol_courses')

    def update_rec_courses(self, instance):
        # Get recommended courses from FS1 API
        student_id = str(instance._id)
        KPs_data = [x.tree_id for x in instance.weak_KPs.all()]
        passed_KPs = ','.join(KPs_data)
        # print(passed_KPs)
        api_url = serverlink + 'api/rec-courses/%s/%s' % (student_id, passed_KPs) #+ '?format=json'
        # print(api_url)
        courses_data = requests.get(api_url).json()
        str_courses = courses_data['rec_courses']
        rec_courses = str_courses[1:-1].replace(" ", "").replace('\'', '').split(",")
        # print(rec_courses)

        # Update rec_courses
        instance.rec_courses.clear()
        for str_id in rec_courses:
            course_id = ObjectId(str_id)
            course = Course.objects.get(_id=course_id)
            instance.rec_courses.add(course)
        instance.save()

    def create(self, validated_data):
        _id = ObjectId(validated_data['_id'])
        student = Student.objects.create(_id=_id)
        student._id = _id

        if 'kp' in list(validated_data.keys()):
            KPs_data = validated_data['kp']
            for obj in KPs_data:
                kp_obj = KP.objects.get(tree_id=obj[0])
                student.weak_KPs.add(kp_obj, through_defaults={'score':obj[1]})
                self.update_rec_courses(student)

        if 'enrol_courses' in list(validated_data.keys()):
            enrol_data = validated_data['enrol_courses']
            for str_id in enrol_data:
                course_id = ObjectId(str_id)
                course = Course.objects.get(_id=course_id)
                student.enrol_courses.add(course)           

        student.save()

        return student

    def update(self, instance, validated_data):
        # Update weak_KPs
        if 'kp' in list(validated_data.keys()):
            KPs_data = validated_data.pop('kp')
            instance.weak_KPs.clear()
            for obj in KPs_data:
                kp_obj = KP.objects.get(tree_id=obj[0])
                instance.weak_KPs.add(kp_obj, through_defaults={'score':obj[1]})
                self.update_rec_courses(instance)

        if 'enrol_courses' in list(validated_data.keys()):
            enrol_data = validated_data['enrol_courses']
            instance.enrol_courses.clear()
            for str_id in enrol_data:
                course_id = ObjectId(str_id)
                course = Course.objects.get(_id=course_id)
                instance.enrol_courses.add(course)   

        instance.save()

        return instance

# {"_id":"507f191e810c19729de860ec", "corr_KPs":["1.9.1.1.1"], "teacher_id":"507f191e810c19729de860eb"}
# {
#     "_id": "507f191e810c19729de86117",
#     "weak_KPs": ["1.9.1.1.2"]
# }