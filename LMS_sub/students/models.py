from djongo import models
# from .utils import list_all_KPs

# KP_CHOICES = [(item[0], item[1]) for item in list_all_KPs()]

#Create your models here.
class KP(models.Model):
    tree_id = models.CharField(default='0', primary_key=True, max_length=100)
    name = models.CharField(max_length=500)
    objects = models.DjongoManager()

    def __str__(self):
        return self.name


class KPSubject(models.Model):
    tree_id = models.CharField(default='0', primary_key=True, max_length=100)
    subject_id = models.IntegerField()
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name

class KPGrade(models.Model):
    parent = models.ForeignKey(KPSubject, on_delete=models.CASCADE, related_name='subset')
    tree_id = models.CharField(default='0', primary_key=True, max_length=100)
    grade_id = models.IntegerField()
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name

class KPArea1L(models.Model):
    parent = models.ForeignKey(KPGrade, on_delete=models.CASCADE, related_name='subset')
    tree_id = models.CharField(default='0', primary_key=True, max_length=100)
    area1L_id = models.IntegerField()
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name

class KPArea2L(models.Model):
    parent = models.ForeignKey(KPArea1L, on_delete=models.CASCADE, related_name='subset')
    tree_id = models.CharField(default='0', primary_key=True, max_length=100)
    area2L_id = models.IntegerField()
    name = models.CharField(max_length=500)
    
    def __str__(self):
        return self.name

class KPArea3L(models.Model):
    parent = models.ForeignKey(KPArea2L, on_delete=models.CASCADE, related_name='subset')
    tree_id = models.CharField(default='0', primary_key=True, max_length=100)
    area3L_id = models.IntegerField()
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name

class Course(models.Model):
    _id = models.ObjectIdField()
    corr_KPs = models.ManyToManyField(KP)
    name = models.CharField(default='', max_length=500)
    duration = models.IntegerField(default=0)
    room = models.CharField(default='', max_length=20)
    price = models.IntegerField(default=0)
    teacher_id = models.GenericObjectIdField()
    start_time = models.CharField(default='', max_length=300)
    schedule = models.JSONField(default=[])


class Student(models.Model):
    _id = models.ObjectIdField()
    weak_KPs = models.ManyToManyField(KP, through='StudentKP')
    rec_courses = models.ManyToManyField(Course, through='StudentRecCourse', related_name='rec_students')
    enrol_courses = models.ManyToManyField(Course, through='StudentEnrolCourse', related_name='enrol_students')

class StudentKP(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='kp', related_query_name='kp')
    tree_id = models.ForeignKey(KP, on_delete=models.CASCADE, related_name='kp', related_query_name='kp')
    score = models.FloatField(default=0)

class StudentRecCourse(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

class StudentEnrolCourse(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

# class CourseKP(models.Model):
#     course = models.ForeignKey(Course, on_delete=models.CASCADE)
#     kp = models.ForeignKey(KP, on_delete=models.CASCADE)


