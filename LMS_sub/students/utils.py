import csv, os
import pymongo
import sqlite3

def load_from_mongodb(database, collection, query, details={}, mode='find'):
    '''
    database: Name of mongodb database
    collection: Name of mongodb collection
    query: Query object
            {'field_name': 'field_value'}    
    -------
    returns pymongo.cursor.Cursor instance
    '''
    client = pymongo.MongoClient('127.0.0.1', 27017)
    db = client.get_database(database)
    coll = db.get_collection(collection)

    if mode == 'find':
        result = coll.find(query, details)
    elif mode == 'distinct':
        result = coll.distinct(query)
    elif mode == 'find_one':
        result = coll.find_one(query, details)
    client.close()
    return result

def list_all_KPs(result):
    KP_list = []
    for doc in result:
        components = list(doc.values())[:-1]
        tree_id = '.'.join([str(x) for x in components])
        name = str(doc['name'])
        KP_list.append([tree_id, name])
    return KP_list

def get_all_KPs():
    KP_list = []
    query, details = {}, {'_id':0}
    colls = ['Subject', 'Grade', 'Area1L', 'Area2L', 'Area3L']
    for coll in colls:
        result = load_from_mongodb('UPA', coll, query, details)
        KP_list = KP_list + list_all_KPs(result)
    return KP_list

# print(get_all_KPs())