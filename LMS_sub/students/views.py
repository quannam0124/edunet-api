from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import KP, KPSubject, KPGrade, KPArea1L, KPArea2L, KPArea3L
from .models import Course
from bson.objectid import ObjectId
from .models import Student
from .serializers import *
import re

# # Create your views here.
@api_view(['GET', 'DELETE', 'POST'])
def students_list(request):
    """
 List  students, or create a new student.
 """
    if request.method == 'GET':
        students = Student.objects.all()
        serializer = StudentDisplaySerializer(students, many=True)

        return Response(serializer.data)

    elif request.method == 'DELETE':
        students = Student.objects.all()
        students.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    elif request.method == 'POST':
        serializer = StudentCreateSerializer(data=request.data)
        if serializer.is_valid():
            saved_obj = serializer.save()
            response_data = StudentDisplaySerializer(saved_obj).data

            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'DELETE', 'PATCH'])
def students_detail(request, pk):
    """
    Retrieve, update or delete a customer by id/pk.
    """
    #try
    student = Student.objects.get(_id=ObjectId(pk))
    # except Student.DoesNotExist:
    #     return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = StudentDisplaySerializer(student,context={'request': request})
        return Response(serializer.data)

    elif request.method == 'DELETE':
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    elif request.method == 'PATCH':
        serializer = StudentCreateSerializer(student, data=request.data, context={'request': request}, partial=True)
        if serializer.is_valid():
            saved_obj = serializer.save()
            response_data = StudentDisplaySerializer(saved_obj).data
            
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def KPs_list(request):
    """
 List  KPs.
 """
    if request.method == 'GET':
        kps = KP.objects.all()
        serializer = KPSerializer(kps, many=True)

        return Response(serializer.data)

@api_view(['GET'])
def KPs_detail(request, tree_id):
    kp = KP.objects.get(tree_id=tree_id)

    if request.method == 'GET':
        serializer = KPSerializer(kp,context={'request': request})
        return Response(serializer.data)
        
@api_view(['GET'])
def KPs_subset(request, tree_id):
    r_exp = r'^'+ re.escape(tree_id) + r'\.[0-9]+$'
    query = {'tree_id': {'$regex': r_exp}}
    
    if request.method == 'GET':
        kps = KP.objects.mongo_find(query)
        serializer = KPSerializer(kps, many=True)

        return Response(serializer.data)

@api_view(['GET'])
def KPs_nested(request):
    if request.method == 'GET':
        kps = KPSubject.objects.all()
        serializer = KPSubjectSerializer(kps, many=True)

        return Response(serializer.data)

@api_view(['GET', 'DELETE', 'POST'])
def courses_list(request):
    if request.method == 'GET':
        courses = Course.objects.all()
        serializer = CourseDisplaySerializer(courses, many=True)

        return Response(serializer.data)

    elif request.method == 'DELETE':
        courses = Course.objects.all()
        courses.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
    
    elif request.method == 'POST':
        serializer = CourseCreateSerializer(data=request.data)
        if serializer.is_valid():
            saved_obj = serializer.save()
            response_data = CourseDisplaySerializer(saved_obj).data

            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE', 'PATCH'])
def courses_detail(request, pk):
    course = Course.objects.get(_id=ObjectId(pk))

    if request.method == 'GET':
        serializer = CourseDisplaySerializer(course,context={'request': request})
        
        return Response(serializer.data)

    elif request.method == 'DELETE':
        course.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    elif request.method == 'PATCH':
        serializer = CourseCreateSerializer(course, data=request.data, context={'request': request}, partial=True)
        if serializer.is_valid():
            saved_obj = serializer.save()
            response_data = CourseDisplaySerializer(saved_obj).data
            
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
