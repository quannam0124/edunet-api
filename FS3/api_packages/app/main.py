from flask import Flask,render_template,request
import diffstep
from diffstep import print_html_steps
from sympy import sympify
#from flask_ngrok import run_with_ngrok
import intStep
from intstep import print_html_steps


app = Flask(__name__)
#run_with_ngrok(app)

@app.route('/')
def home():
    return '<h1>API For Diffferentiation Module</h1>'

@app.route('/diff', methods = ['POST', 'GET'])
def diffsolve():
    if ('expr' and 'sym') in request.args:
        expr = sympify(request.args['expr'].replace(' ', '+'))
        sym = sympify(request.args['sym'])
        return '''<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
                  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>''' + diffstep.print_html_steps(expr, sym)
    else:
        return "Error: No expr or sym field provided. Please specify an expr and a sym."

@app.route('/int', methods = ['POST', 'GET'])
def intsolve():
    if ('expr' and 'sym') in request.args:
        expr = sympify(request.args['expr'].replace(' ', '+'))
        sym = sympify(request.args['sym'])
        return '''<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
                  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>''' + intstep.print_html_steps(expr, sym)
    else:
        return "Error: No expr or sym field provided. Please specify an expr and a sym."

if __name__ == "__main__":
  app.run()
