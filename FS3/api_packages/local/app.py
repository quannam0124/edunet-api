from flask import Flask,render_template,request
import diffstep
from diffstep import print_html_steps
from sympy import sympify
from flask_ngrok import run_with_ngrok
##import intStep

app = Flask(__name__)
run_with_ngrok(app)

@app.route('/')
def form():
    return render_template('form.html')

@app.route('/solution/', methods = ['POST', 'GET'])
def data():
    if request.method == 'GET':
        return f"The URL /solution is accessed directly. Try going to '/form' to submit form"
    if request.method == 'POST':
        form_data = request.form
        return '''<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
                  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
                  </script>''' + print_html_steps(sympify(form_data['Expression']), sympify(form_data['Symbol']))

app.run()
